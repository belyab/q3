package src.itis.socialtest.entities;

import java.util.List;

public class Post {

    private String date;
    private String content;
    private Long likesCount;
    private Author author;
    private Long authorId;

    public Post(String date, String content, Long likesCount, Author author) {
        this.date = date;
        this.content = content;
        this.likesCount = likesCount;
        this.author = author;
    }

    public Post(String[] split, List<Author> allAuthors) {
        this.date = split[2];
        this.likesCount = Long.parseLong(split[1]);
        this.authorId = Long.parseLong(split[0]);
        this.content = split[3];
        for (int i = 4; i < split.length; i++) {
            this.content = this.content + ", " + split[i];
        }
        this.author = allAuthors.stream().filter(author -> author.getId().equals(authorId)).findAny().get();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Post{" +
                "date=" + date +
                ", content=" + content +
                ", likesCount=" + likesCount +
                ", author=" + author.toString() +
                "}";
    }
}
