package src.itis.socialtest;


import src.itis.socialtest.entities.Author;
import src.itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;
    private List<Author> allAuthors;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws FileNotFoundException {
        new MainClass().run("src/itis/socialtest/resources/PostDatabase.csv", "src/itis/socialtest//resources/Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) throws FileNotFoundException {
        File postsFile = new File(postsSourcePath);
        File authorsFile = new File(authorsSourcePath);

        allAuthors = new BufferedReader(new FileReader(authorsFile))
                .lines()
                .map(f -> new Author(f.split(", ")))
                .collect(Collectors.toList());
        allPosts = new BufferedReader(new FileReader(postsFile))
                .lines()
                .map(f -> new Post(f.split(", "), allAuthors))
                .collect(Collectors.toList());
        for (Post post : allPosts) {
            System.out.println(post.toString());
        }
        List<Post> findPostsByDate = analyticsService.findPostsByDate(allPosts, "17.04.2021");
        for (Post post : findPostsByDate) {
            System.out.println("Посты за сегодня: " + post.toString());
        }
        List<Post> findAllPostsByAuthorNickname = analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov");
        for (Post post : findAllPostsByAuthorNickname) {
            System.out.println("Пост varlamov: " + post.toString());
        }
        boolean checkPostsThatContainsSearchString = analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия");
        System.out.println(" содержит ли текст хотя бы одного поста слово \"Россия\": " + checkPostsThatContainsSearchString);
    }
}
